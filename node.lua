hosted_init()

gl.setup(1920, 1080)
node.alias "departures" -- catch all communication
local json = require 'json'
local loader = require "loader"

local res = util.auto_loader()


util.auto_loader(_G)
function table.filter(t, predicate)
    local j = 1

    for i, v in ipairs(t) do
        if predicate(v) then
            t[j] = v
            j = j + 1
        end
    end

    while t[j] ~= nil do
        t[j] = nil
        j = j + 1
    end

    return t
end

function draw_lauri(gfx, x, y, rot, alpha)
    local w, h = gfx:size()
    alpha = alpha or 1
    gl.pushMatrix()
        gl.translate(x + w/2, y + w/2, 0)
        if rot then
            gl.rotate(rot, 0, 0, 1)
        end
        gl.translate(-(x + w/2), -(y + w/2), 0)
        gfx:draw(x, y, x + w, y + h, alpha)
    gl.popMatrix()
end

function raingenerator()
    local drops = {}
    local last = sys.now()
    local rate = 1

    local function draw()
        local now = sys.now()
        local delta = now - last
        last = now

        for idx, drop in ipairs(drops) do
            draw_lauri(_G[drop.img], drop.x, drop.y)
            drop.x = drop.x - 30 * delta
            drop.y = drop.y + drop.speed * delta
            drop.speed = drop.speed * 1.01
        end

        -- rausgescrollte entfernen
        drops = table.filter(drops, function(drop)
            return drop.y < 550
        end)

    end

    local function add_drop(x, y)
        local drop_type = math.floor(math.random() * 7) + 1
        table.insert(drops, {
            x = x;
            y = y;
            speed = 130 + math.random() * 30;
            img = string.format("weather_drop%d", drop_type);
        })
    end

    return {
        draw = draw;
        add_drop = add_drop;
    }
end

function cloudgenerator(rain)
    local clouds = {}
    local last = sys.now()
    local factors = {0,0,0,0}

    local function draw()
        local now = sys.now()
        local delta = now - last
        last = now

        for idx, cloud in ipairs(clouds) do
            draw_lauri(res[cloud.img],
                cloud.x, cloud.y,
                math.sin(sys.now() * cloud.freq + cloud.phase) * cloud.rot
            )
            cloud.x = cloud.x - cloud.speed * delta
            if cloud.img == "weather_cloud4" and math.random() < 0.1 then
                rain.add_drop(cloud.x + 30 + math.random() * 250, cloud.y + 120)
            end
        end

        -- rausgescrollte entfernen
        clouds = table.filter(clouds, function(cloud)
            return cloud.x > -400
        end)

        for cloud_type = 1, 4 do
            local should_generate = math.random() * 100 < factors[cloud_type] and #clouds < 10
            if should_generate then
                table.insert(clouds, {
                    x = WIDTH;
                    y = -50 + math.random() * 300;
                    speed = 40 + math.random() * 30;
                    freq = math.random() / 4;
                    phase = math.random() * 2;
                    rot = math.random() * 2;
                    img = string.format("weather_cloud%d", cloud_type);
                })
            end
        end
    end

    local function set_factors(new_factors)
        factors = new_factors
    end

    return {
        draw = draw;
        set_factors = set_factors;
    }
end

local rc = raingenerator()
local cc = cloudgenerator(rc)

local forecasts
local conditions

util.file_watch("conditions.json", function(content)
    print("loading conditions")
    conditions = json.decode(content).current_observation
    local cloud_mapping = {
        clear = {0, 0, 0, 0};
        sunny = {0, 0, 0, 0};
        partlycloudy = {0.2, 0.2, 0.2, 0};
        mostlycloudy = {0.5, 0.5, 0.5, 0};
        cloudy = {1, 1, 1, 0};
        mist = {0.1, 0.1, 0.1, 0.3};
        rain = {0.1, 0.1, 0.1, 1};
        snow = {0.5, 0.5, 0.5, 0};
    }
    local icon = conditions.icon
    local factors = cloud_mapping[icon]
    if not factors then
        error("no mapping for " .. icon)
    else
        cc.set_factors(factors)
    end
end)

util.file_watch("forecast.json", function(content)
    print("loading forecasts")
    forecasts = json.decode(content).forecast.simpleforecast.forecastday
end)

local shader = resource.create_shader[[
    precision mediump float;
    uniform sampler2D Texture;
    varying vec2 TexCoord;
    uniform float bright;
    void main() {
        vec4 texel = texture2D(Texture, TexCoord.st);
        gl_FragColor = texel * mix(vec4(1,1,1,1), vec4(0.3, 0.3, 0.7, 1), bright);
    }
]]



local base_time = N.base_time or 0
local day = 0
local year = 0
local month = ""
local INTERVAL = 20
util.data_mapper{
    ["clock/set"] = function(time)
        base_time = tonumber(time) - sys.now()
        N.base_time = base_time
    end;
    ["day/set"] = function(time)
        day = time
    end;
    ["year/set"] = function(time)
        year = time
    end;
    ["month/set"] = function(time)
        month = time
    end;
}

util.resource_loader{
    "izquierda.png",
    "derecha.png",
    "playlist.txt",
}




-- enough time to load next image
local SWITCH_DELAY = 1

-- transition time in seconds.
-- set it to 0 switching instantaneously
local SWITCH_TIME = 1.0


-- Media directory. Set to '' to have your
-- images in the current directory.
-- local MEDIA_DIRECTORY = ''
local MEDIA_DIRECTORY = 'a'

----------------------------------------------------------------


local ALL_CONTENTS, ALL_CHILDS = node.make_nested()

assert(SWITCH_TIME + SWITCH_DELAY < INTERVAL,
    "INTERVAL must be longer than SWITCH_DELAY + SWITCHTIME")

gl.setup(NATIVE_WIDTH, NATIVE_HEIGHT)

local function alphanumsort(o)
    local function padnum(d) return ("%03d%s"):format(#d, d) end
    table.sort(o, function(a,b)
        return tostring(a):gsub("%d+",padnum) < tostring(b):gsub("%d+",padnum)
    end)
    return o
end

local pictures = util.generator(function()
    local files = {}
    for name, _ in pairs(ALL_CONTENTS[MEDIA_DIRECTORY]) do
        if name:match(".*jpg") or name:match(".*png") then
            files[#files+1] = name
        end
    end
    return alphanumsort(files) -- sort files by filename
end)

node.event("content_remove", function(filename)
    pictures:remove(filename)
end)

local current_image = resource.create_colored_texture(0,0,0,0)
local fade_start = 0

local function next_image()
    local next_image_name = pictures.next()
    print("now loading " .. next_image_name)
    last_image = current_image
    current_image = resource.load_image(next_image_name)
    fade_start = sys.now()
end


local playlistz, video, current_video_idx

    current_video_idx = 0

util.file_watch("playlist.txt", function(content)
    playlistz = {}
    for filename in string.gmatch(content, "[^\r\n]+") do
        playlistz[#playlistz+1] = filename
    end
    print("new playlist")
    pp(playlist)
end)

function next_video()

    if current_video_idx >= #playlistz then --esta linea puede ser igual igual
        current_video_idx = 1
    else
        current_video_idx = current_video_idx + 1
    end

    if video then
        video:dispose()
    end

    video = util.videoplayer(playlistz[current_video_idx], {loop=false,audio=true})


end


function node.render()
    gl.clear(0, 0, 0, 1)
    local time = (base_time + sys.now()) % 86400
    local dia = day
    local mes = month
    local anio= year
    local hour = (time / 3600) % 12
    local minute = time % 3600 / 60
    local second = time % 60
    local textoPM = ""

    if((time / 3600) % 24)<12 then
        textoPM="AM"
    else
        textoPM="PM"
    end

    if math.floor(hour) == 0 then
      hour = 12
    end

    local delta = sys.now() - fade_start - SWITCH_DELAY

    if last_image and delta < 0 then
        last_image:draw(0, 0, 1920, HEIGHT)
    elseif last_image and delta < SWITCH_TIME then
        local progress = delta / SWITCH_TIME
        last_image:draw(0, 0, 1920, HEIGHT, 1 - progress)
        current_image:draw(0, 0, 1920, HEIGHT, progress)
    else
        if last_image then
            last_image:dispose()
            last_image = nil
        end
        current_image:draw(0, 0, 1920, HEIGHT)
    end

end
util.set_interval(INTERVAL, next_image)
