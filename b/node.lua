-- at which intervals should the screen switch to the
-- next image?
node.alias "b"
local INTERVAL = 20


-- enough time to load next image
local SWITCH_DELAY = 1

-- transition time in seconds.
-- set it to 0 switching instantaneously
local SWITCH_TIME = 1.0

-- Media directory. Set to '' to have your
-- images in the current directory.
-- local MEDIA_DIRECTORY = ''
local MEDIA_DIRECTORY = ''

----------------------------------------------------------------

-- local ALL_CONTENTS, ALL_CHILDS = node.make_nested()

assert(SWITCH_TIME + SWITCH_DELAY < INTERVAL,
    "INTERVAL must be longer than SWITCH_DELAY + SWITCHTIME")

gl.setup(690, 510)

local hh=510
local ww=690

local function alphanumsort(o)
    local function padnum(d) return ("%03d%s"):format(#d, d) end
    table.sort(o, function(a,b)
        return tostring(a):gsub("%d+",padnum) < tostring(b):gsub("%d+",padnum)
    end)
    return o
end

local imagenes = util.generator(function()
    local files = {}
    for name, _ in pairs(CONTENTS) do
        if name:match(".*jpg") or name:match(".*png") then
            files[#files+1] = name
        end
    end
    return alphanumsort(files) -- sort files by filename
end)

node.event("content_remove", function(filename)
    imagenes:remove(filename)
end)

local current_image = resource.create_colored_texture(0,0,0,0)
local fade_start = 0

local function next_image()
    local next_image_name = imagenes.next()
    print("now loading " .. next_image_name)
    last_image = current_image
    current_image = resource.load_image(next_image_name)
    fade_start = sys.now()
end


function node.render()
gl.clear(1, 1, 1, 1)
    local delta = sys.now() - fade_start - SWITCH_DELAY
    if last_image and delta < 0 then
        last_image:draw(0, 0, ww, hh)
    elseif last_image and delta < SWITCH_TIME then
        local progress = delta / SWITCH_TIME
        last_image:draw(0, 0, ww, hh, 1 - progress)
        current_image:draw(0, 0, ww, hh, progress)
    else
        if last_image then
            last_image:dispose()
            last_image = nil
        end
        current_image:draw(0, 0, ww, hh)
    end
end
util.set_interval(INTERVAL, next_image)
